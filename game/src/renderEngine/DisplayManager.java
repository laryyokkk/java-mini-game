package renderEngine;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryUtil;

public class DisplayManager {

    public long windowID;

    public void createDisplay(int width, int height, String title){
        boolean initState = GLFW.glfwInit();

        if (!initState){
            throw new IllegalStateException("Could not create GLFW");
        }

        windowID = GLFW.glfwCreateWindow(width, height, title, MemoryUtil.NULL, MemoryUtil.NULL);

        if (windowID == MemoryUtil.NULL){
            throw new IllegalStateException("Can not make window");
        }

        GLFW.glfwMakeContextCurrent(windowID);
        GLFW.glfwSwapInterval(1);
        GLFW.glfwShowWindow(windowID);
        GL.createCapabilities();
    }
    public void updateDisplay(){
        GLFW.glfwSwapBuffers(windowID);
        GLFW.glfwPollEvents();
        GLFW.glfwSwapInterval(1);
    }

    public void closeDisplay(){
        GLFW.glfwDestroyWindow(windowID);
        GLFW.glfwTerminate();
    }

    public boolean shouldClose(){
        return GLFW.glfwWindowShouldClose(windowID);
    }

}
