package main;

import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.RawModel;
import renderEngine.Renderer;

public class MainApp {
    public static void main(String[] args) {
        DisplayManager display = new DisplayManager();
        display.createDisplay(1280,720,"Test");

        Loader loader = new Loader();
        Renderer renderer = new Renderer();

        float[] vertices = {
                -0.5f, 0.5f, 0f,
                -0.5f, -0.5f, 0f,
                0.5f, -0.5f, 0f,
                0.5f, -0.5f, 0f,
                0.5f, 0.5f, 0f,
                -0.5f, 0.5f, 0f
        };

        RawModel model = loader.loadToVAO(vertices);

        while (!display.shouldClose()){
            renderer.prepare();
            renderer.render(model);
            display.updateDisplay();
        }

        loader.cleanUp();
        display.closeDisplay();
    }
}
